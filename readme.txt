Spitfire -- This software is provided 'as-is' without any express or
implied warranty. Read the full license agreement in "license.txt".


Spitfire is a high level framework that is built on top of SFML and
NetEXT. Spitfire is designed to provide specific classes that are
directly related to building games.

The included classes deal with event handling, state management,
UI controls, and sprite batching. The following list is intended
to provide a general overview, it is far from a complete list.


*Spitfire.ScreenManager.ScreenManagerBase - Abstract base class for
   defining a state manager. Each state manager holds full control
   of the screen and the active manager will recieve any events that
   are generated from the window.

*Spitfire.Application.Controller - Provides a game loop with management
   for the state managers. The game loop has a fixed time step and will
   automatically pass events along to the current state manager.

*Spitfire.UI.ControlBase - A base class for defining custom controls.
   Each control has multiple properties, including size, position,
   z-order, child controls, and more. Controls are organized in a tree
   like structure with each control relative to its parent.

*Spitfire.UI.ScreenManagerUIBase - A specialized state manager abstract
   base class intended for directly containing controls. All events are
   automatically passed along directly to the parent control which in turn
   passes the events to all the child controls.

*Spitfire.Resources.ResourceManagerBase - A basic resource manager interface to avoid
   loading of duplicate resources.