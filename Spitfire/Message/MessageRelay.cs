﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Spitfire.Message
{
    public class MessageRelay
    {
        #region Variables
        private Mutex _lock = new Mutex(false);
        private List<MessageBus> _buslist = new List<MessageBus>();
        #endregion

        #region Properties
        public MessageBus[] BusList
        {
            get
            {
                return _buslist.ToArray();
            }
        }
        #endregion

        #region Functions
        public void RegisterBus(MessageBus Bus)
        {
            _lock.WaitOne();
            if (_buslist.Contains(Bus)) return;
            _buslist.Add(Bus);
            _lock.ReleaseMutex();
        }
        public void UnregisterBus(MessageBus Bus)
        {
            _lock.WaitOne();
            _buslist.Remove(Bus);
            _lock.ReleaseMutex();
        }
        public void RelayMessage(object Sender, object Args)
        {
            _lock.WaitOne();
            foreach (var bus in _buslist)
            {
                bus.SendMessage(Sender, Args);
            }
            _lock.ReleaseMutex();
        }
        #endregion
    }
}
