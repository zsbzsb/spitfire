﻿using System;
using NetEXT.Concurrent;

namespace Spitfire.Message
{
    public class MessageBus
    {
        #region Variables
        private Dispatcher _dispatcher = null;
        private Delegate _senddelegate = null;
        #endregion

        #region Events
        public event Action<object, object> MessageSent;
        #endregion

        #region Constructors
        public MessageBus() : this(Dispatcher.CurrentDispatcher) { }
        public MessageBus(Dispatcher Dispatcher)
        {
            _dispatcher = Dispatcher;
            _senddelegate = new Action<object, object>(OnSendMessage);
        }
        #endregion

        #region Functions
        public void SendMessage(object Sender, object Args)
        {
            _dispatcher.InvokeAsync(_senddelegate, new object[] { Sender, Args }, false);
        }
        private void OnSendMessage(object Sender, object Args)
        {
            if (MessageSent != null) MessageSent(Sender, Args);
        }
        #endregion
    }
}
