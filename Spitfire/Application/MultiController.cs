﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;
using NetEXT.TimeFunctions;
using Spitfire.ScreenManager;
using Spitfire.Message;

namespace Spitfire.Application
{
    public class MultiController
    {
        #region Variables
        private Dictionary<RenderWindow, List<ScreenManagerBase>> _windowscreenmanagers = new Dictionary<RenderWindow, List<ScreenManagerBase>>();
        private Dictionary<RenderWindow, EventHandlerGroup> _windowevents = new Dictionary<RenderWindow, EventHandlerGroup>();
        private MessageBus _bus = new MessageBus();
        private Time _timestep = Time.Zero;
        private Color _clearcolor = Color.Black;
        private bool _stoploop = false;
        private CloseMode _mode = CloseMode.Window;
        #endregion

        #region Properties
        public Time TimeStep
        {
            get
            {
                return _timestep;
            }
            set
            {
                _timestep = value;
            }
        }
        public Color ClearColor
        {
            get
            {
                return _clearcolor;
            }
            set
            {
                _clearcolor = value;
            }
        }
        public bool StopLoop
        {
            get
            {
                return _stoploop;
            }
            set
            {
                _stoploop = value;
            }
        }
        #endregion

        #region Events
        public event Action<MultiController> FrameUpdate;
        #endregion

        #region Constructors
        public MultiController(Time TimeStep, MessageRelay Relay = null, CloseMode Mode = CloseMode.Window)
        {
            _timestep = TimeStep;
            _mode = Mode;
            _bus.MessageSent += MessageRecieved;
            if (Relay != null) Relay.RegisterBus(_bus);
        }
        #endregion

        #region Functions
        public void RegisterWindow(RenderWindow Window, ScreenManagerBase InitialScreenManager, CloseMode Mode = CloseMode.Window)
        {
            _windowscreenmanagers.Add(Window, new List<ScreenManagerBase>(new ScreenManagerBase[] { InitialScreenManager }));
            InitialScreenManager.SwitchScreen += OnSwitchScreen;
            InitialScreenManager.CloseScreen += OnCloseScreen;
            InitialScreenManager.ScreenActivated();
            BindWindowEvents(Window, Mode);
        }
        public void UnregisterWindow(RenderWindow Window)
        {
            UnbindWindowEvents(Window);
            _windowscreenmanagers.Remove(Window);
        }
        private void BindWindowEvents(RenderWindow Window, CloseMode Mode)
        {
            var group = new EventHandlerGroup();
            group.KeyPressed = (sender, e) => { _windowscreenmanagers[Window][_windowscreenmanagers[Window].Count - 1].KeyPressed(Window, e); };
            group.KeyReleased = (sender, e) => { _windowscreenmanagers[Window][_windowscreenmanagers[Window].Count - 1].KeyReleased(Window, e); };
            group.MouseMoved = (sender, e) => { _windowscreenmanagers[Window][_windowscreenmanagers[Window].Count - 1].MouseMoved(Window, e); };
            group.ButtonPressed = (sender, e) => { _windowscreenmanagers[Window][_windowscreenmanagers[Window].Count - 1].MouseButtonPressed(Window, e); };
            group.ButtonReleased = (sender, e) => { _windowscreenmanagers[Window][_windowscreenmanagers[Window].Count - 1].MouseButtonReleased(Window, e); };
            if (Mode == CloseMode.Window) group.Closed = (sender, e) => { Window.Close(); };
            else if (Mode == CloseMode.Screen) group.Closed = (sender, e) => { _windowscreenmanagers[Window][_windowscreenmanagers[Window].Count - 1].CloseRequested(); };
            Window.KeyPressed += group.KeyPressed;
            Window.KeyReleased += group.KeyReleased;
            Window.MouseMoved += group.MouseMoved;
            Window.MouseButtonPressed += group.ButtonPressed;
            Window.MouseButtonReleased += group.ButtonReleased;
            if (group.Closed != null) Window.Closed += group.Closed;
            _windowevents.Add(Window, group);
        }
        private void UnbindWindowEvents(RenderWindow Window)
        {
            var group = _windowevents[Window];
            Window.KeyPressed -= group.KeyPressed;
            Window.KeyReleased -= group.KeyReleased;
            Window.MouseMoved -= group.MouseMoved;
            Window.MouseButtonPressed -= group.ButtonPressed;
            Window.MouseButtonReleased -= group.ButtonReleased;
            if (group.Closed != null) Window.Closed -= group.Closed;
            _windowevents.Remove(Window);
        }
        private void OnSwitchScreen(RenderWindow Window, ScreenManagerBase NewScreenManager)
        {
            if (_windowscreenmanagers[Window].Contains(NewScreenManager)) return;
            _windowscreenmanagers[Window][_windowscreenmanagers[Window].Count - 1].ScreenDeactivated();
            _windowscreenmanagers[Window][_windowscreenmanagers[Window].Count - 1].SwitchScreen -= OnSwitchScreen;
            _windowscreenmanagers[Window][_windowscreenmanagers[Window].Count - 1].CloseScreen -= OnCloseScreen;
            _windowscreenmanagers[Window].Add(NewScreenManager);
            _windowscreenmanagers[Window][_windowscreenmanagers[Window].Count - 1].SwitchScreen += OnSwitchScreen;
            _windowscreenmanagers[Window][_windowscreenmanagers[Window].Count - 1].CloseScreen += OnCloseScreen;
            _windowscreenmanagers[Window][_windowscreenmanagers[Window].Count - 1].ScreenActivated();
        }
        private void OnCloseScreen(RenderWindow Window)
        {
            _windowscreenmanagers[Window][_windowscreenmanagers[Window].Count - 1].ScreenDeactivated();
            _windowscreenmanagers[Window][_windowscreenmanagers[Window].Count - 1].SwitchScreen -= OnSwitchScreen;
            _windowscreenmanagers[Window][_windowscreenmanagers[Window].Count - 1].CloseScreen -= OnCloseScreen;
            _windowscreenmanagers[Window].RemoveAt(_windowscreenmanagers[Window].Count - 1);
            if (_windowscreenmanagers[Window].Count <= 0) Window.Close();
            else
            {
                _windowscreenmanagers[Window][_windowscreenmanagers[Window].Count - 1].SwitchScreen += OnSwitchScreen;
                _windowscreenmanagers[Window][_windowscreenmanagers[Window].Count - 1].CloseScreen += OnCloseScreen;
                _windowscreenmanagers[Window][_windowscreenmanagers[Window].Count - 1].ScreenActivated();
            }
        }
        public void RunLoop()
        {
            Clock frameclock = new Clock();
            Time elapsedtime = Time.Zero;
            while (!_stoploop)
            {
                List<RenderWindow> removelist = null;
                elapsedtime += frameclock.Restart();
                int stepcount = 0;
                while (elapsedtime >= TimeStep)
                {
                    elapsedtime -= TimeStep;
                    stepcount += 1;
                }
                foreach (var window in _windowscreenmanagers.Keys)
                {
                    window.DispatchEvents();
                    if (!window.IsOpen())
                    {
                        if (removelist == null) removelist = new List<RenderWindow>();
                        removelist.Add(window);
                        continue;
                    }
                    window.Clear(_clearcolor);
                    for (int i = 0; i < stepcount; i++)
                    {
                        _windowscreenmanagers[window][_windowscreenmanagers[window].Count - 1].Update(TimeStep);
                    }
                    if (_windowscreenmanagers[window].Count >= 1) _windowscreenmanagers[window][_windowscreenmanagers[window].Count - 1].Draw(window);
                    window.Display();
                }
                if (FrameUpdate != null) FrameUpdate(this);
                if (removelist != null)
                {
                    foreach (var window in removelist)
                    {
                        UnregisterWindow(window);
                    }
                    if (_windowevents.Keys.Count == 0) _stoploop = true;
                }
            }
            _stoploop = false;
        }
        private void MessageRecieved(object Sender, object Args)
        {
            foreach (var managers in _windowscreenmanagers.Values)
            {
                if (managers.Count > 0)
                {
                    managers[managers.Count - 1].MessageRecieved(Sender, Args);
                }
            }
        }
        #endregion

        #region Additional Definitions
        private struct EventHandlerGroup
        {
            public EventHandler<KeyEventArgs> KeyPressed;
            public EventHandler<KeyEventArgs> KeyReleased;
            public EventHandler<MouseMoveEventArgs> MouseMoved;
            public EventHandler<MouseButtonEventArgs> ButtonPressed;
            public EventHandler<MouseButtonEventArgs> ButtonReleased;
            public EventHandler Closed;
        }
        #endregion
    }
}
