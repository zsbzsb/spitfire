﻿using System;

namespace Spitfire.Application
{
    public enum CloseMode
    {
        Screen,
        Window,
        None
    }
}
