﻿using System;
using System.Threading;
using NetEXT.Concurrent;
using Spitfire.Message;

namespace Spitfire.Application
{
    public class ThreadedController
    {
        #region Variables
        private Controller _controller = null;
        private Thread _controllerthread = null;
        private bool _running = false;
        private Dispatcher _dispatcher = null;
        private MessageBus _bus = null;
        private object _tag = null;
        #endregion

        #region Properties
        public MessageBus Bus
        {
            get
            {
                return _bus;
            }
        }
        public object Tag
        {
            get
            {
                return _tag;
            }
            set
            {
                _tag = value;
            }
        }
        #endregion

        #region Events
        public event Func<ThreadedController, Controller> GetController;
        #endregion

        #region Constructors
        public ThreadedController()
        {
            _controllerthread = new Thread(new ParameterizedThreadStart(ThreadEntry));
        }
        #endregion

        #region Functions
        public void StartThread() { StartThread(null); }
        public void StartThread(MessageRelay Relay)
        {
            if (_running) return;
            _running = true;
            _controllerthread.Start(Relay);
        }
        public void StopThread()
        {
            if (!_running) return;
            _controller.StopLoop = true;
        }
        private void ThreadEntry(object Relay)
        {
            if (GetController == null)
            {
                _running = false;
                return;
            }
            _dispatcher = Dispatcher.CurrentDispatcher;
            _bus = new MessageBus(_dispatcher);
            if (Relay != null) ((MessageRelay)Relay).RegisterBus(_bus);
            _bus.MessageSent += MessageRecieved;
            _controller = GetController(this);
            _controller.FrameUpdate += FrameUpdate;
            _controller.RunLoop();
            _controller.FrameUpdate -= FrameUpdate;
            _controller = null;
            _bus.MessageSent -= MessageRecieved;
            if (Relay != null) ((MessageRelay)Relay).UnregisterBus(_bus);
            _bus = null;
            _dispatcher = null;
            _running = false;
        }
        private void FrameUpdate(Controller Sender)
        {
            _dispatcher.HandleCallbacks();
        }
        private void MessageRecieved(object Sender, object Args)
        {
            var manager = _controller.ActiveScreenManager;
            if (manager == null) return;
            manager.MessageRecieved(Sender, Args);
        }
        #endregion
    }
}
