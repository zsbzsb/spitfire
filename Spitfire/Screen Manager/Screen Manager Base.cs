﻿using System;
using SFML.Window;
using SFML.Graphics;
using NetEXT.TimeFunctions;
using NetEXT.Concurrent;
using Spitfire.Resources;

namespace Spitfire.ScreenManager
{
    public abstract class ScreenManagerBase
    {
        #region Variables
        private bool _autoclose = true;
        private bool _enablemusic = true;
        private RenderWindow _window = null;
        #endregion

        #region Properties
        public bool AutoClose
        {
            get
            {
                return _autoclose;
            }
            set
            {
                _autoclose = value;
            }
        }
        public bool EnableMusic
        {
            get
            {
                return _enablemusic;
            }
            set
            {
                _enablemusic = value;
            }
        }
        internal RenderWindow Window
        {
            get
            {
                return _window;
            }
            set
            {
                _window = value;
            }
        }
        #endregion

        #region Events
        internal event Action<RenderWindow, ScreenManagerBase> SwitchScreen;
        internal event Action<RenderWindow> CloseScreen;
        #endregion

        #region Functions
        public virtual void KeyPressed(RenderTarget Target, KeyEventArgs EventArgs) { }
        public virtual void KeyReleased(RenderTarget Target, KeyEventArgs EventArgs) { }
        public virtual void MouseButtonPressed(RenderTarget Target, MouseButtonEventArgs EventArgs) { }
        public virtual void MouseButtonReleased(RenderTarget Target, MouseButtonEventArgs EventArgs) { }
        public virtual void MouseMoved(RenderTarget Target, MouseMoveEventArgs EventArgs) { }
        public virtual void ScreenActivated() { }
        public virtual void ScreenDeactivated() { }
        public virtual void CloseRequested() { if (_autoclose) OnCloseScreen(); }
        protected void OnSwitchScreen(ScreenManagerBase NewScreen) { if (SwitchScreen != null) SwitchScreen(_window, NewScreen); }
        protected void OnCloseScreen() { if (CloseScreen != null) CloseScreen(_window); }
        public virtual void Update(Time DeltaTime) { if (_enablemusic) MusicManager.Update(DeltaTime); }
        public abstract void Draw(RenderTarget Target);
        public virtual void MessageRecieved(object Sender, object Args) { }
        #endregion
    }
}
