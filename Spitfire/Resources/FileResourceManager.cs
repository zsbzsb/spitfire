﻿using System;
using System.IO;

namespace Spitfire.Resources
{
    public class FileResourceManager : ResourceManagerBase
    {
        #region Functions
        protected override Stream LoadResource(string Path)
        {
            return new FileStream(Path, FileMode.Open, FileAccess.Read);
        }
        #endregion
    }
}
