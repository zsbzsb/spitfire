﻿using System;
using System.Collections.Generic;
using System.IO;
using SFML.Window;
using SFML.Graphics;
using SFML.Audio;

namespace Spitfire.Resources
{
    public abstract class ResourceManagerBase
    {
        #region Variables
        private Dictionary<string, Texture> _texturecache = new Dictionary<string, Texture>();
        private Dictionary<string, Font> _fontcache = new Dictionary<string, Font>();
        private Dictionary<string, SoundBuffer> _soundbuffercache = new Dictionary<string, SoundBuffer>();
        private Dictionary<string, Stream> _resourcecache = new Dictionary<string, Stream>();
        #endregion

        #region Functions
        protected abstract Stream LoadResource(string Path);
        public Texture GetTexture(string TexturePath)
        {
            if (_texturecache.ContainsKey(TexturePath)) return _texturecache[TexturePath];
            _texturecache.Add(TexturePath, new Texture(LoadResource(TexturePath)));
            _texturecache[TexturePath].Smooth = true;
            return _texturecache[TexturePath];
        }
        public Font GetFont(string FontPath)
        {
            if (_fontcache.ContainsKey(FontPath)) return _fontcache[FontPath];
            _fontcache.Add(FontPath, new Font(LoadResource(FontPath)));
            return _fontcache[FontPath];
        }
        public SoundBuffer GetSoundBuffer(string SoundBufferPath)
        {
            if (_soundbuffercache.ContainsKey(SoundBufferPath)) return _soundbuffercache[SoundBufferPath];
            _soundbuffercache.Add(SoundBufferPath, new SoundBuffer(LoadResource(SoundBufferPath)));
            return _soundbuffercache[SoundBufferPath];
        }
        public Stream GetResource(string ResourcePath)
        {
            if (_resourcecache.ContainsKey(ResourcePath)) return _resourcecache[ResourcePath];
            _resourcecache.Add(ResourcePath, LoadResource(ResourcePath));
            return _resourcecache[ResourcePath];
        }
        #endregion
    }
}
