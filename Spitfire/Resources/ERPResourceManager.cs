﻿using System;
using System.IO;
using ERP.FileControl;
using ERP.VFS;

namespace Spitfire.Resources
{
    public class ERPResourceManager : ResourceManagerBase
    {
        #region Variables
        private PackedFile _file = null;
        #endregion

        #region Constructors
        public ERPResourceManager(string FilePath)
        {
            _file = VFSLoader.LoadPackedFile(FilePath);
        }
        #endregion

        #region Functions
        protected override Stream LoadResource(string Path)
        {
            return _file.LocateFile(Path).FileData;
        }
        #endregion
    }
}
