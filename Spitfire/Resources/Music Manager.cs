﻿using System;
using System.Collections.Generic;
using System.IO;
using SFML.Window;
using SFML.Audio;
using NetEXT.TimeFunctions;
using NetEXT.MathFunctions;

namespace Spitfire.Resources
{
    public static class MusicManager
    {
        #region Variables
        private static ResourceManagerBase _manager = null;
        private static List<string> _queuedtracks = new List<string>();
        private static int _playingindex = 0;
        private static Music _playingmusic = null;
        private static bool _shuffle = true;
        private static float _volume = 40;
        #endregion

        #region Properties
        public static ResourceManagerBase Manager
        {
            get
            {
                return _manager;
            }
            set
            {
                _manager = value;
            }
        }
        public static bool Shuffle
        {
            get
            {
                return _shuffle;
            }
        }
        public static float Volume
        {
            get
            {
                return _volume;
            }
            set
            {
                _volume = value;
                if (_playingmusic != null) _playingmusic.Volume = _volume;
            }
        }
        #endregion

        #region Functions
        internal static void Update(Time DeltaTime)
        {
            if (_playingmusic != null)
            {
                if (_playingmusic.Status == SoundStatus.Stopped)
                {
                    _playingmusic.Dispose();
                    _playingmusic = null;
                    if (_shuffle) _playingindex = RandomGenerator.Random(0, _queuedtracks.Count - 1);
                    else _playingindex += 0;
                    if (_playingindex > _queuedtracks.Count - 1) _playingindex = 0;
                    _playingmusic = new Music(_manager.GetResource(_queuedtracks[_playingindex]));
                    _playingmusic.Loop = false;
                    _playingmusic.Volume = _volume;
                    _playingmusic.Play();
                }
            }
        }
        public static void QueueSong(string MusicPath)
        {
            _queuedtracks.Add(MusicPath);
        }
        public static void Play()
        {
            if (_playingmusic == null && _queuedtracks.Count > 0)
            {
                if (_shuffle) _playingindex = RandomGenerator.Random(0, _queuedtracks.Count - 1);
                else _playingindex = 0;
                _playingmusic = new Music(_manager.GetResource(_queuedtracks[_playingindex]));
                _playingmusic.Loop = false;
                _playingmusic.Volume = _volume;
                _playingmusic.Play();
            }
            else if (_playingmusic != null && _playingmusic.Status == SoundStatus.Paused) _playingmusic.Play();
        }
        public static void Pause()
        {
            if (_playingmusic != null && _playingmusic.Status == SoundStatus.Playing) _playingmusic.Pause();
        }
        public static void Stop()
        {
            if (_playingmusic != null && _playingmusic.Status == SoundStatus.Paused || _playingmusic.Status == SoundStatus.Playing)
            {
                _playingmusic.Stop();
                _playingmusic.Dispose();
                _playingmusic = null;
            }
        }
        #endregion
    }
}
