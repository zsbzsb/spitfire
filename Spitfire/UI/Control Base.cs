﻿using System;
using System.Collections.Generic;
using NetEXT.TimeFunctions;
using SFML.Window;
using SFML.Graphics;

namespace Spitfire.UI
{
    public class ControlBase : Drawable
    {
        #region Variables
        private Vector2f _position = new Vector2f(0, 0);
        private Vector2f _size = new Vector2f(0, 0);
        private bool _enabled = true;
        private int _zorder = 0;
        private ControlBase _parent = null;
        private List<ControlBase> _children = new List<ControlBase>();
        private ControlBase _focusedcontrol = null;
        private ControlBase _lastovercontrol = null;
        private object _tag = null;
        private bool _recievechildevents = false;
        private View _customview = null;
        #endregion

        #region Properties
        public virtual Vector2f Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }
        public virtual Vector2f Size
        {
            get
            {
                return _size;
            }
            set
            {
                _size = value;
            }
        }
        public virtual bool Enabled
        {
            get
            {
                return _enabled;
            }
            set
            {
                _enabled = value;
            }
        }
        public int ZOrder
        {
            get
            {
                return _zorder;
            }
            set
            {
                _zorder = value;
                if (_parent != null)
                {
                    _parent.RemoveChild(this);
                    _parent.AddChild(this);
                }
            }
        }
        public ControlBase Parent
        {
            get
            {
                return _parent;
            }
            set
            {
                if (_parent != value)
                {
                    if (_parent != null) { _parent.RemoveChild(this); }
                    _parent = value;
                    if (_parent != null) { _parent.AddChild(this); }
                }
            }
        }
        public ControlBase[] Children
        {
            get
            {
                return _children.ToArray();
            }
        }
        public virtual object Tag
        {
            get
            {
                return _tag;
            }
            set
            {
                _tag = value;
            }
        }
        public bool RecieveChildEvents
        {
            get
            {
                return _recievechildevents;
            }
            set
            {
                _recievechildevents = value;
            }
        }
        public View CustomView
        {
            get
            {
                return _customview;
            }
            set
            {
                _customview = value;
            }
        }
        #endregion

        #region Events
        public event Action<ControlBase> ControlMouseEnter;
        public event Action<ControlBase> ControlMouseLeave;
        public event Action<ControlBase> ControlGotFocus;
        public event Action<ControlBase> ControlLostFocus;
        #endregion

        #region Functions
        public void AddChild(ControlBase Child)
        {
            if (_children.Contains(Child)) return;
            _children.Add(Child);
            Child.Parent = this;
            _children.Sort(new Comparison<ControlBase>((LHS, RHS) => { return LHS.ZOrder.CompareTo(RHS.ZOrder); }));
        }
        public void RemoveChild(ControlBase Child)
        {
            if (!_children.Contains(Child)) return;
            if (_focusedcontrol == Child) { _focusedcontrol = null; }
            if (_lastovercontrol == Child) { _lastovercontrol = null; }
            _children.Remove(Child);
            Child.Parent = null;
        }
        public virtual void Update(Time DeltaTime)
        {
            foreach (ControlBase control in _children)
            {
                control.Update(DeltaTime);
            }
        }
        public virtual void Draw(RenderTarget Target, RenderStates States)
        {
            States.Transform.Translate(_position);
            View view = FindView();
            View oldview = null;
            if (view != null)
            {
                oldview = new View(Target.GetView());
                Target.SetView(view);
            }
            foreach (ControlBase control in _children)
            {
                control.Draw(Target, States);
            }
            if (view != null) Target.SetView(oldview);
        }
        protected Vector2f GetMouseRelativePosition(Vector2f MousePosition)
        {
            Vector2f currentpos = MousePosition - _position;
            if (_parent != null) currentpos = _parent.GetMouseRelativePosition(currentpos);
            return currentpos;
        }
        protected View FindView()
        {
            if (_customview != null) return _customview;
            else if (_parent != null) return _parent.FindView();
            else return null;
        }
        public void OnKeyPressed(RenderTarget Target, KeyEventArgs EventArgs)
        {
            if (_focusedcontrol != null)
            {
                if (_focusedcontrol != this) _focusedcontrol.OnKeyPressed(Target, EventArgs);
                else
                {
                    if (Parent != null && Parent.RecieveChildEvents) Parent.KeyPressed(Target, EventArgs);
                    KeyPressed(Target, EventArgs);
                }
            }
            else
            {
                if (Parent != null && Parent.RecieveChildEvents) Parent.KeyPressed(Target, EventArgs);
                KeyPressed(Target, EventArgs);
            }
        }
        public void OnKeyReleased(RenderTarget Target, KeyEventArgs EventArgs)
        {
            if (_focusedcontrol != null)
            {
                if (_focusedcontrol != this) _focusedcontrol.OnKeyReleased(Target, EventArgs);
                else
                {
                    if (Parent != null && Parent.RecieveChildEvents) Parent.KeyReleased(Target, EventArgs);
                    KeyReleased(Target, EventArgs);
                }
            }
            else
            {
                if (Parent != null && Parent.RecieveChildEvents) Parent.KeyReleased(Target, EventArgs);
                KeyReleased(Target, EventArgs);
            }
        }
        public void OnMouseButtonPressed(RenderTarget Target, MouseButtonEventArgs EventArgs)
        {
            Vector2f mappedcoords = new Vector2f(0, 0);
            View view = FindView();
            if (view != null) mappedcoords = Target.MapPixelToCoords(new Vector2i(EventArgs.X, EventArgs.Y), view);
            else mappedcoords = Target.MapPixelToCoords(new Vector2i(EventArgs.X, EventArgs.Y));
            Vector2f mousepos = GetMouseRelativePosition(mappedcoords);
            ControlBase controlover = null;
            int i = _children.Count - 1;
            while (i >= 0)
            {
                if (_children[i].Position.X <= mousepos.X && _children[i].Position.Y <= mousepos.Y && _children[i].Position.X + _children[i].Size.X >= mousepos.X && _children[i].Position.Y + _children[i].Size.Y >= mousepos.Y)
                {
                    if (_children[i].Enabled)
                    {
                        controlover = _children[i];
                        break;
                    }
                }
                i -= 1;
            }
            if (controlover != null) // We are over a child control
            {
                if (controlover != _focusedcontrol) // Over a different child control
                {
                    controlover.OnMouseButtonPressed(Target, EventArgs);
                    if (_focusedcontrol != null) _focusedcontrol.OnLostFocus();
                    _focusedcontrol = controlover;
                }
                else // Over the same child control
                {
                    _focusedcontrol.OnMouseButtonPressed(Target, EventArgs);
                }
            }
            else // Over ourself
            {
                if (this != _focusedcontrol) // Was over a child control the last time
                {
                    GotFocus();
                    EventArgs.X = (int)Math.Round(mappedcoords.X, 0);
                    EventArgs.Y = (int)Math.Round(mappedcoords.Y, 0);
                    MouseButtonPressed(Target, EventArgs);
                    if (_focusedcontrol != null) _focusedcontrol.OnLostFocus();
                    _focusedcontrol = null;
                }
                else
                {
                    EventArgs.X = (int)Math.Round(mappedcoords.X, 0);
                    EventArgs.Y = (int)Math.Round(mappedcoords.Y, 0);
                    MouseButtonPressed(Target, EventArgs);
                }
            }
        }
        public void OnMouseButtonReleased(RenderTarget Target, MouseButtonEventArgs EventArgs)
        {
            if (_focusedcontrol != this && _focusedcontrol != null)
            {
                _focusedcontrol.OnMouseButtonReleased(Target, EventArgs);
            }
            else
            {
                Vector2f mappedcoords = new Vector2f(0, 0);
                View view = FindView();
                if (view != null) mappedcoords = Target.MapPixelToCoords(new Vector2i(EventArgs.X, EventArgs.Y), view);
                else mappedcoords = Target.MapPixelToCoords(new Vector2i(EventArgs.X, EventArgs.Y));
                EventArgs.X = (int)Math.Round(mappedcoords.X, 0);
                EventArgs.Y = (int)Math.Round(mappedcoords.Y, 0);
                MouseButtonReleased(Target, EventArgs);
            }
        }
        public void OnMouseMoved(RenderTarget Target, MouseMoveEventArgs EventArgs)
        {
            Vector2f mappedcoords = new Vector2f(0, 0);
            View view = FindView();
            if (view != null) mappedcoords = Target.MapPixelToCoords(new Vector2i(EventArgs.X, EventArgs.Y), view);
            else mappedcoords = Target.MapPixelToCoords(new Vector2i(EventArgs.X, EventArgs.Y));
            Vector2f mousepos = GetMouseRelativePosition(mappedcoords);
            ControlBase controlover = null;
            int i = _children.Count - 1;
            while (i >= 0)
            {
                if (_children[i].Position.X <= mousepos.X && _children[i].Position.Y <= mousepos.Y && _children[i].Position.X + _children[i].Size.X >= mousepos.X && _children[i].Position.Y + _children[i].Size.Y >= mousepos.Y)
                {
                    if (_children[i].Enabled)
                    {
                        controlover = _children[i];
                        break;
                    }
                }
                i -= 1;
            }
            if (controlover != null) // We are over a child control
            {
                if (controlover != _lastovercontrol) // Over a different child control
                {
                    controlover.OnMouseMoved(Target, EventArgs);
                    if (_lastovercontrol != null) _lastovercontrol.OnMouseLeave();
                    _lastovercontrol = controlover;
                }
                else // Over the same child control
                {
                    _lastovercontrol.OnMouseMoved(Target, EventArgs); // Will trigger a mouse moved
                }
            }
            else // Over ourself
            {
                if (this != _lastovercontrol) // Was over a child control the last time
                {
                    MouseEnter();
                    EventArgs.X = (int)Math.Round(mappedcoords.X, 0);
                    EventArgs.Y = (int)Math.Round(mappedcoords.Y, 0);
                    if (Parent != null && Parent.RecieveChildEvents) Parent.MouseMoved(Target, EventArgs);
                    MouseMoved(Target, EventArgs);
                    if (_lastovercontrol != null) _lastovercontrol.OnMouseLeave();
                    _lastovercontrol = this;
                }
                else
                {
                    EventArgs.X = (int)Math.Round(mappedcoords.X, 0);
                    EventArgs.Y = (int)Math.Round(mappedcoords.Y, 0);
                    if (Parent != null && Parent.RecieveChildEvents) Parent.MouseMoved(Target, EventArgs);
                    MouseMoved(Target, EventArgs);
                }
            }
        }
        public void OnMouseLeave()
        {
            if (_lastovercontrol != null)
            {
                if (_lastovercontrol != this)
                {
                    _lastovercontrol.OnMouseLeave();
                    _lastovercontrol = null;
                }
                else
                {
                    this.MouseLeave();
                    _lastovercontrol = null;
                }
            }
            else
            {
                this.MouseLeave();
            }
        }
        public void OnLostFocus()
        {
            if (_lastovercontrol != null)
            {
                if (_lastovercontrol != this)
                {
                    _lastovercontrol.OnLostFocus();
                    _lastovercontrol = null;
                }
                else
                {
                    this.LostFocus();
                    _lastovercontrol = null;
                }
            }
            else
            {
                this.LostFocus();
            }
        }
        public ControlBase FindChild(object Tag)
        {
            foreach (ControlBase control in _children)
            {
                if (control.Tag != null && control.Tag.Equals(Tag)) return control;
            }
            return null;
        }
        public void FocusControl(ControlBase ChildControl)
        {
            if (!_children.Contains(ChildControl)) return;
            if (ChildControl != _focusedcontrol)
            {
                ChildControl.GotFocus();
                if (_focusedcontrol != null) _focusedcontrol.OnLostFocus();
                _focusedcontrol = ChildControl;
            }
        }
        public virtual void KeyPressed(RenderTarget Target, KeyEventArgs EventArgs) { }
        public virtual void KeyReleased(RenderTarget Target, KeyEventArgs EventArgs) { }
        public virtual void MouseButtonPressed(RenderTarget Target, MouseButtonEventArgs EventArgs) { }
        public virtual void MouseButtonReleased(RenderTarget Target, MouseButtonEventArgs EventArgs) { }
        public virtual void MouseEnter() { if (ControlMouseEnter != null) ControlMouseEnter(this); }
        public virtual void MouseLeave() { if (ControlMouseLeave != null) ControlMouseLeave(this); }
        public virtual void MouseMoved(RenderTarget Target, MouseMoveEventArgs EventArgs) { }
        public virtual void GotFocus() { if (ControlGotFocus != null) ControlGotFocus(this); }
        public virtual void LostFocus() { if (ControlLostFocus != null) ControlLostFocus(this); }
        #endregion
    }
}
