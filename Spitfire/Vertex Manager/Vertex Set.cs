﻿using System;
using SFML.Window;
using SFML.Graphics;

namespace Spitfire.VertexManager
{
    public class VertexSet
    {
        #region Variables
        private Vertex[] _vertices = new Vertex[4];
        private VertexSetContainer _container = null;
        private int _existingindex = -1;
        #endregion

        #region Properties
        public Vertex this[int i]
        {
            get
            {
                if (i < 0 || i > 3) throw new IndexOutOfRangeException();
                return _vertices[i];
            }
            set
            {
                _vertices[i] = value;
                _container.VertexSetUpdated(this);
            }
        }
        public int ExistingIndex
        {
            get
            {
                return _existingindex;
            }
            set
            {
                _existingindex = value;
            }
        }
        #endregion

        #region Constructors
        public VertexSet(VertexSetContainer Container)
        {
            _container = Container;
            _container.AddVertexSet(this);
        }
        #endregion

        #region Functions
        public void RemoveFromParent()
        {
            _container.RemoveVertexSet(this);
        }
        #endregion
    }
}
